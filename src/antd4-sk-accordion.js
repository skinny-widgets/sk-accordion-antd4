
import { AntdSkAccordion }  from '../../sk-accordion-antd/src/antd-sk-accordion.js';

export class Antd4SkAccordion extends AntdSkAccordion {

    get prefix() {
        return 'antd4';
    }

}
